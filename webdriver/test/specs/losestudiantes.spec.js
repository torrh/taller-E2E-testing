var assert = require('assert');

describe('Login los estudiantes ', function () {
    it('Deberia notificar error de cuenta ya existente', function () {

        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.waitForEnabled('button=Ingresar', 5000);
        browser.scroll('button=Ingresar');
        browser.click('button=Ingresar');

        browser.waitForVisible('input[name="nombre"]', 5000);
        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys('Servio');

        var apellidoInput = cajaSignUp.element('input[name="apellido"]');
        apellidoInput.click();
        apellidoInput.keys('Pantoja');

        var emailInput = cajaSignUp.element('input[name="correo"]');
        emailInput.click();
        emailInput.keys('shd_cristo@hotmail.com');

        var selectUniversidad = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversidad.selectByVisibleText("Universidad de los Andes");

        var selectPrograma = cajaSignUp.element('select[name="idPrograma"]');
        selectPrograma.selectByVisibleText("Administración");

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('ServioPantoja');

        var checkAceptar = cajaSignUp.element('input[name="acepta"]');
        checkAceptar.click();

        var buttonAceptar = cajaSignUp.element('.logInButton');
        buttonAceptar.click();

        browser.waitForVisible('.sweet-alert', 10000);
        var alertText = browser.element('.sweet-alert h2').getText();
        expect(alertText).toBe('Ocurrió un error activando tu cuenta');

    });
});
